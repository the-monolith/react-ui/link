import { component, React, Style } from 'local/react/component'

import { navigate } from 'local/react/router'

export const defaultLinkStyle: Style = {
  marginRight: '0.15em',
  alignSelf: 'center'
}

export const Link = component.children
  .props<{ external?: boolean; to: string; className?: string; style?: Style }>(
    {
      external: false,
      style: defaultLinkStyle
    }
  )
  .render(({ external, children, className = 'link', style, to }) => (
    <a
      href={to}
      style={style}
      className={className}
      onClick={external ? undefined : navigate(to)}
      target={external ? '_blank' : '_self'}
    >
      {children}
    </a>
  ))
